import styled from 'styled-components';

export const Container = styled.div`
    display: flex;
    justify-content: space-between;
    align-items: center;

    margin-top:80px;
    
`;

export const Card = styled.div`
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2);
    transition: 0.3s;
    padding: 10px;

    max-width: 200px;
`;

export const Title = styled.h3`
    text-align: center;
    padding-bottom: 2px;
    border-bottom: 1px solid red;
`;

export const Price = styled.p`
    
`;
