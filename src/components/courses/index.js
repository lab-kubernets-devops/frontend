import React, { useState, useEffect } from 'react';
import { Container, Card, Title, Price } from './style';
import CourseService from './../../services/course.service';

const Courses = () => {

    const [courses, setCourses] = useState([]);

    useEffect(() => {
        getCourses();
    }, [])

    const getCourses = async () => {
        try {
            let response = await CourseService.getCourses();

            if (response.status === 200) {
                setCourses(response.data);
            }

        } catch (error) {
            console.log(error);
        }
    }

    return (
        <>
            <Container>
                {courses.map(course =>
                    <Card key={course.id}>
                        <Title>{course.title}</Title>
                        <p>{course.description}</p>
                        <Price>Valor: R$ {course.price.value}</Price>
                    </Card>

                )}
            </Container>
        </>
    )

}

export default Courses;