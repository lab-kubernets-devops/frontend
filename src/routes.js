import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import IndexPage from './pages/main';
import CoursePage from './components/courses';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={IndexPage} />
    </Switch>
  </BrowserRouter>
);

export default Routes;