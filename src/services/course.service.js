import Course from "../components/courses";
import api from './base/api'

class CourseService{

    async getCourses(limit=100){
        return api.get(`/courses?limit=${limit}`);
    }
}

export default new CourseService();