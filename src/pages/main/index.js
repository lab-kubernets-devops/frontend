import React from 'react';
import { Container } from './style';
import Courses from './../../components/courses';

const IndexPage = () => {

    return (
        <>
            <Container>
                <h1>Bem vendo a loja-lab</h1>
            </Container>
            <Courses />
        </>

    )
}

export default IndexPage;