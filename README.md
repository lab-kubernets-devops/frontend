### Build container image

- docker build -t image-name .

### Push image

- docker push <image-name:tag>